package com.example.swimmingpoolpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
//fungsi kelas  ini adalah untuk menyimpan atribut-atrbut dari data pembayran yang akan
//kita masukkan, dan juga beberapa method getter dan setter
public class konfirm extends AppCompatActivity {

    private String tanggal;
    private String waktu;
    private String jumlah;
    private String total;

    public konfirm() {

    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJumlahOrang() {
        return jumlah;
    }

    public void setJumlahOrang(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return " " + tanggal + "\n" +
                " " + waktu + "\n" +
                " " + jumlah + "\n" +
                " " + total;
    }

    public konfirm(String tgl, String wkt, String jml, String tal) {
        tanggal = tgl;
        waktu = wkt;
        jumlah = jml;
        total = tal;
    }
}