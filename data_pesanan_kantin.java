package com.example.te_wallet;

public class data_pesanan_kantin {
    String kn_nama;
    String kn_pesanan;
    String kn_tambahan;
    int kn_total_harga;

    public data_pesanan_kantin(String nama, String pesanan, String tambahan, int total_harga) {
        this.kn_nama = nama;
        this.kn_pesanan = pesanan;
        this.kn_tambahan = tambahan;
        this.kn_total_harga = total_harga;
    }

    public String getNama() {
        return kn_nama;
    }

    public void setNama(String nama) {
        this.kn_nama = nama;
    }

    public String getPesanan() {
        return kn_pesanan;
    }

    public void setPesanan(String pesanan) {
        this.kn_pesanan = pesanan;
    }

    public String getTambahan() {
        return kn_tambahan;
    }

    public void setTambahan(String tambahan) {
        this.kn_tambahan = tambahan;
    }

    public int getTotal_harga() {
        return kn_total_harga;
    }

    public void setTotal_harga(int total_harga) {
        this.kn_total_harga = total_harga;
    }
}
