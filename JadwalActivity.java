package com.example.swimmingpoolpayment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Ref;

public class JadwalActivity extends AppCompatActivity {


    private Button btSubmit;
    private EditText tanggal;
    private EditText waktu;
    private EditText jumlah;
    private EditText total;

    FirebaseDatabase database;
    //variable yang merefers ke firebase realtime database
    DatabaseReference Refer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal2);

        tanggal = findViewById(R.id.tanggal);
        waktu = findViewById(R.id.waktu);
        jumlah = findViewById(R.id.jumlah);
        total = findViewById(R.id.total);
        btSubmit = findViewById(R.id.tambah);

        Refer = FirebaseDatabase.getInstance().getReference();
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isEmpty(tanggal.getText().toString()) && !isEmpty(waktu.getText().toString()) && !isEmpty(jumlah.getText().toString()) && !isEmpty(total.getText().toString()))
                    submitbayar(new konfirm(tanggal.getText().toString(), waktu.getText().toString(), jumlah.getText().toString(), total.getText().toString()));
                else
                    Snackbar.make(findViewById(R.id.tambah), "Data tidak boleh kosong", Snackbar.LENGTH_LONG).show();

                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        total.getWindowToken(), 0);
            }
        });
    }

            private boolean isEmpty(String s) {
                return TextUtils.isEmpty(s);

            }

            private void submitbayar(final konfirm konfirm) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference Refer = database.getReference("Bayar Renang");

                Refer.child("Tanggal").setValue(tanggal.getText().toString());
                Refer.child("Waktu").setValue(waktu.getText().toString());
                Refer.child("Jumlah").setValue(jumlah.getText().toString());
                Refer.child("Total").setValue(total.getText().toString());

                Refer.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Snackbar.make(findViewById(R.id.tambah), "Data berhasil ditambahkan", Snackbar.LENGTH_LONG).show();
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value

                    }
                });
            }


            public void kembali2(View view) {
                Intent list = new Intent(JadwalActivity.this, MemberActivity.class);
                startActivity(list);
            }
        }
