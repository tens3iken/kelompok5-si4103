package com.example.te_wallet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;

public class detail_transaksi_kantin extends AppCompatActivity {
    int quantity=0;
    String menutambahan = " ";
    String menuutama;
    int harga=12000;
    private DatabaseReference database;
    data_user data_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi_kantin);

        Intent intent = getIntent();
        menuutama = intent.getStringExtra(daftar_menu_kantin.EXTRA_MESSAGE_1);

        //menghubungkan database
        database = FirebaseDatabase.getInstance().getReference();
    }

    public void increment(View view){//perintah tombol tambah
        if(quantity==100){
            Toast.makeText(this,"pesanan maximal 100",Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity+1 ;
        display(quantity);
    }
    public void decrement(View view){//perintah tombol tambah
        if (quantity==1){
            Toast.makeText(this,"pesanan minimal 1",Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity -1;
        display(quantity);
    }


    public void Submitorder(View view) {
        EditText nameEditText=(EditText)findViewById(R.id.edt_name);
        String name=nameEditText.getText().toString();
        Log.v("MainActivity","Nama:"+name);

        CheckBox EsTehChekBox= (CheckBox) findViewById(R.id.Esteh_checkbox);
        boolean hasEsTeh=EsTehChekBox.isChecked();//mengidentifikasi check
        Log.v("MainActivity","has whippedcream:"+hasEsTeh);

        CheckBox EsJerukChekBox= (CheckBox) findViewById(R.id.EsJeruk_checkbox);
        boolean hasEsJeruk=EsJerukChekBox.isChecked();//mengidentifikasi check
        Log.v("MainActivity","has whippedcream:"+hasEsJeruk);

        CheckBox BajigurChekBox= (CheckBox) findViewById(R.id.Bajigur_checkbox);
        boolean hasBajigur=BajigurChekBox.isChecked();//mengidentifikasi check
        Log.v("MainActivity","has whippedcream:"+hasBajigur);

        int price=calculateprice(hasEsTeh,hasEsJeruk);//memanggil method jumlah harga
        String pricemessage=createOrderSummary(price,name,hasEsTeh,hasEsJeruk);


        displayMessage(pricemessage);

        final String user = SharePreferences.getLoggedInUser(getBaseContext());
        data_pesanan_kantin pesanan = new data_pesanan_kantin(
                                        user,
                                        menuutama,
                                        menutambahan,
                                        harga
        );
        //menambahkan data user ke database
        database.child("Kantin").push().setValue(pesanan);

        //testing ambil single data
        database.child("Data User").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String mnama,mnim,musername,memail,mpass;
                int saldodata;
                for (DataSnapshot notedataSnapshot: dataSnapshot.getChildren()) {
                    mnama = notedataSnapshot.child("du_nama").getValue().toString();
                    mnim = notedataSnapshot.child("du_nim").getValue().toString();
                    musername = notedataSnapshot.child("du_username").getValue().toString();
                    memail = notedataSnapshot.child("du_email").getValue().toString();
                    mpass = notedataSnapshot.child("du_pass").getValue().toString();
                    saldodata = Integer.parseInt(notedataSnapshot.child("du_saldo").getValue().toString());


                    if (user.equals(mnama)){
                            int saldo_akhir = saldodata - harga;
                            //menyimpan data yang dari database ke local
                            data_user = new data_user(
                                    mnama,
                                    mnim,
                                    musername,
                                    memail,
                                    mpass,
                                    saldo_akhir
                            );
                            break;
                    }else{
                    }
                    break;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        database.child("Data User").orderByChild("du_nama").equalTo(user).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot update: dataSnapshot.getChildren()){
                    update.getRef().setValue(data_user);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        startActivity(new Intent(this, menu_utama.class));
    }

    private int calculateprice(boolean addEsTeh,boolean addEsJeruk){//jumlah pesanan * harga
        if(addEsTeh){
            menutambahan = menutambahan + " Es Teh";
            harga=harga+3000;//harga tambahan toping
        }

        if (addEsJeruk){
            menutambahan = menutambahan +" Es Jeruk";
            harga=harga+5000;
        }

        return quantity * harga;
    }
    private String createOrderSummary(int price, String name, boolean addEsTeh, boolean addEsJeruk) {//hasil pemesanan
        String pricemessage=" Nama      : "+name;
        pricemessage+="\n Tambah      : Es Teh? "+addEsTeh;
        pricemessage+="\n Tambah      : Es Jeruk? "+addEsJeruk;
        pricemessage+="\n Jumlah      : "+quantity+" Porsi";
        pricemessage+="\n Total Rp    : "+price;
        pricemessage+="\n TERIMA KASIH";
        return  pricemessage;
    }

    //method ini untuk mencetak hasil perintah yang di tampilkan dengan inisial quantity_textview di textview 0
    private void displayMessage(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.price_textview);
        priceTextView.setText(message);
    }
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_textview);
        quantityTextView.setText("" + number);
    }
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_textview);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }
}
