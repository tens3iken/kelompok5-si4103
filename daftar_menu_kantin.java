package com.example.te_wallet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class daftar_menu_kantin extends AppCompatActivity {
    public static final String EXTRA_MESSAGE_1 = "MESSAGE_1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_menu_kantin);
    }

    public void menu_pesan_1(View view) {
        Intent pesan=new Intent(this, detail_transaksi_kantin.class);
        pesan.putExtra(EXTRA_MESSAGE_1, "Ayam Bakar");
        startActivity(pesan);
    }

    public void menu_pesan_2(View view) {
        Intent pesan=new Intent(this, detail_transaksi_kantin.class);
        pesan.putExtra(EXTRA_MESSAGE_1, "Soto Ayam");
        startActivity(pesan);
    }

    public void menu_pesan_3(View view) {
        Intent pesan=new Intent(this, detail_transaksi_kantin.class);
        pesan.putExtra(EXTRA_MESSAGE_1, "Tongseng");
        startActivity(pesan);
    }
}
