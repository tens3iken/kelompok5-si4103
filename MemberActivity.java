package com.example.swimmingpoolpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MemberActivity extends AppCompatActivity{
    private RadioGroup opsigroup;
    private RadioButton memberbutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member2);
        addListenerOnButton();
        }


    public void back1(View view) {
        Intent back = new Intent(this, MainActivity.class);
        startActivity(back);
    }

    public void next1(View view) {
            //get selected radio button from radioGroup
        int selectedId = opsigroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        memberbutton = (RadioButton) findViewById(selectedId);
        Toast.makeText(MemberActivity.this,
                memberbutton.getText(), Toast.LENGTH_SHORT).show();


        Intent in = new Intent(MemberActivity.this, JadwalActivity.class);
        startActivity(in);

    }

    private void addListenerOnButton() {

        opsigroup = (RadioGroup) findViewById(R.id.opsi);
    }

}

